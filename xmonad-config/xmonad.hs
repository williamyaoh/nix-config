{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName

import XMonad.Util.Run (spawnPipe)
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Actions.UpdatePointer

import XMonad.Layout.MultiColumns
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing

import System.IO

import Data.Default

-- Boldface font defined with .xmobarrc
boldface :: Int
boldface = 1

-- Small font defined with .xmobarrc
smallface :: Int
smallface = 2

layout = smartBorders $
  (avoidStruts $
    spacing 8 (multiCol [1] 2 0.01 0.4))
  ||| smartBorders Full

main = do
  xmobarp <- spawnPipe "@xmobar@/bin/xmobar"

  xmonad $ def {
    manageHook = manageDocks <+> manageHook defaultConfig,
    layoutHook = layout,
    logHook = do dynamicLogWithPP defaultPP {
                   ppCurrent = xmobarColor "#e8e8e8" "" . xmobarFont boldface,
                   ppHidden = xmobarFont smallface,
                   ppOrder = \(ws:_:_:_) -> [ws],
                   ppOutput = hPutStrLn xmobarp
                   },
    handleEventHook = docksEventHook <+> handleEventHook def,
    startupHook = setWMName "LG3D",  -- hack to make Java Swing apps play nice
    modMask = mod4Mask,
    clickJustFocuses = False,
    workspaces = ["work", "web", "study"] ++ map show [4..9],
    focusFollowsMouse = False,
    borderWidth = 2,
    normalBorderColor = "#555555",
    focusedBorderColor = "#555555"
  } `additionalKeysP`
    [("M-p", spawn "@dmenu@/bin/dmenu_run")]

-- Wrap the string in code to use the given XMobar font.
-- Depends heavily on the underlying XMobar configuration.
xmobarFont :: Int -> String -> String
xmobarFont i s = wrap ("<fn=" ++ show i ++ ">") "</fn>" s
