{ stdenv }:

{ dmenu, xmobar }:

stdenv.mkDerivation rec {
  name = "xmonad-config-${version}-user";
  version = "0.2.1";

  builder = ./builder.sh;

  config = ./xmonad.hs;
  inherit dmenu xmobar;
}
