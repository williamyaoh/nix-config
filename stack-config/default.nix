{ stdenv }:

stdenv.mkDerivation rec {
  name = "stack-config-${version}-user";
  version = "0.0.1";

  builder = ./builder.sh;

  config = ./config.yaml;
}
