pkgs:

let version = "0.9.14";
in pkgs.buildEnv {
  name = "user-environment-${version}";
  paths = with pkgs; [
    emacs neovim neovim-config

    firefoxWrapper

    transmission

    git
    gitconfig

    strace                      # trace syscalls made by program
    ltrace                      # trace library calls made by program
    lsof                        # look at open file descriptors
    gdb

    ledger                      # command-line double-entry accounting

    dropbox                     # it's just too useful

    unzip
    p7zip
    ripgrep
    jq                          # JSON pretty printer and CLI tool
    file
    tree                        # command-line directory structure viewer
    xclip                       # work with clipboard from command line
    loc                         # lines of code counter
    fd                          # better find
    xsv                         # csv analysis

    evince                      # PDF viewer
    gwenview                    # image viewer
    bc                          # CLI calculator

    ngrok                       # open localhost to the world securely

    gcc
    gnumake
    cmake
    # binutils

    # Haskell stuff.
    # cabal-install
    # stack
    stack-config
  ];
}
