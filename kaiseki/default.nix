{ stdenv, fetchFromGitLab, rustPlatform, pandoc }:

with rustPlatform;

buildRustPackage rec {
  name = "kaiseki-${version}";
  version = "0.2.2";

  src = fetchFromGitLab {
    owner = "williamyaoh";
    repo = "kaiseki";
    rev = "${version}";
    sha256 = "1slq3dzagngpmw7j5w4j8cl01l42ix7mhx4pgq0g33dv8bkgshz2";
  };

  depsSha256 = "034pmd1fmdiyfdlygyqnvnahhhcxk6il46sc0pivmcwgzsgj038s";

  preFixup = ''
    mkdir -p $out/share/man/man1
    cp $src/doc/kaiseki.1 $out/share/man/man1/
  '';

  meta = with stdenv.lib; {
    description = "Unintrusive literate programming preprocessor";
    license = with licenses; [ bsd3 ];
    maintainers = "William Yao <williamyaoh@gmail.com>";
    platforms = platforms.all;
  };
}
