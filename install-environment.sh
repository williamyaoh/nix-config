#!/bin/sh

# Yes, I know that relying on `sh' being there is an ugly hack
# under Nix. But given that Dolstra himself said in his thesis
# that it's something that NixOS can't really work around yet,
# I'm comfortable relying on it being there for now.

nix-env -i user-environment

# Create symlinks from $HOME to all the files under
# `~/.nix-profile/user-environment/'
copy_user_file() {
  # If we're linking a file, we can assume that we've already
  # created the directory it's supposed to live in.
  FILE_RELATIVE_PATH=${1#~/.nix-profile/user-environment}
  if [ -n "$FILE_RELATIVE_PATH" ]; then
    FILE_RELATIVE_PATH=${FILE_RELATIVE_PATH#/}
    if [ -d "$1" ]; then
      mkdir -p "$HOME/$FILE_RELATIVE_PATH"
    elif [ -f "$1" ]; then
      ln -sf "$1" "$HOME/$FILE_RELATIVE_PATH"
    fi
  fi
}

export -f copy_user_file

echo "creating dot file symlinks in home directory"

find -L ~/.nix-profile/user-environment -exec /bin/sh -c 'copy_user_file "$0"' '{}' ';'
			
