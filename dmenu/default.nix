# Produces a `dmenu_run` that runs with my preferred settings.

{ stdenv, dmenu, bash }:

stdenv.mkDerivation rec {
  name = "dmenu-user-${version}";
  version = "0.0.1";

  builder = ./builder.sh;

  dmenuScript = ''
    #!${bash}/bin/bash

    ${dmenu}/bin/dmenu_run -i -p '>' -fn 'Droid Sans:size=9:bold' -nb '#222222' -nf '#e8e8e8'
  '';
  passAsFile = [ "dmenuScript "];
}