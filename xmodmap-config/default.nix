{ stdenv, xmodmap }:

stdenv.mkDerivation rec {
  inherit xmodmap;

  name = "xmodmap-config-${version}-user";
  version = "0.0.1";
  config = ./Xmodmap;
  builder = ./builder.sh;
}