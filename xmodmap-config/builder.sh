source $stdenv/setup

mkdir -p $out/user-environment

# Not a fan of using `.xprofile' this way, because other derivations
# might also want to put commands into it. A more robust method would
# be to build a derivation which combines all of them together.
echo "$xmodmap/bin/xmodmap $config &" > $out/user-environment/.xprofile
