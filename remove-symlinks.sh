#!/bin/sh

# Yes, I know that relying on `sh' being there is an ugly hack
# under Nix. But given that Dolstra himself said in his thesis
# that it's something that NixOS can't really work around yet,
# I'm comfortable relying on it being there for now.

SYMLINK_DIRS=
SYMLINK_PREFIX=~/.nix-profile/user-environment/

remove_nix_symlink() {
  CONTENTS=$(readlink "$1")
  if [ "$CONTENTS" != "${CONTENTS#$SYMLINK_PREFIX}" ]; then
    echo "removing symlink: $1"
    rm -f "$1"
    SYMLINK_DIRS="$(dirname "$1"):$SYMLINK_DIRS"
  fi
}

is_empty() {
  DIRECTORY_CONTENTS=$(ls -A "$1" | wc -l)
  if [ "$DIRECTORY_CONTENTS" = "0" ]; then
    return 0
  else
    return 1
  fi
}

maybe_remove_symlink_dir() {
  if is_empty "$1"; then
    echo "removing empty symlink dir: $1"
    rmdir "$1"
    maybe_remove_symlink_dir $(dirname "$1")
  fi
}

echo "removing created symlinks in home directory"

for f in $(find "$HOME" -mindepth 1 -type l -print); do
  remove_nix_symlink "$f"
done

IFS=:
for dir in $SYMLINK_DIRS; do
  maybe_remove_symlink_dir "$dir"
done
