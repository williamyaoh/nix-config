source $stdenv/setup

mkdir -p $out/bin

# use `find' to get absolute paths
for f in $(find -L $emacs/bin -mindepth 1 -maxdepth 1 -print); do
  OUTPUT_SYMLINK="$out/bin/${f#$emacs/bin/}"
  echo "creating symlink: $OUTPUT_SYMLINK"
  echo "  --> $f"

  ln -s "$f" "$OUTPUT_SYMLINK"
done

mkdir -p $out/user-environment

cp -r $emacsd $out/user-environment/.emacs.d

substituteInPlace $out/user-environment/.emacs.d/packages.el \
  --subst-var sbcl
