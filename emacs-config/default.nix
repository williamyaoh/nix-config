{ stdenv, emacsPackagesNgGen }:

{ emacs, sbcl }:

let emacsWithPackages = (emacsPackagesNgGen emacs).emacsWithPackages;

in stdenv.mkDerivation {
  name = "emacs-${emacs.version}-user";

  emacsd = ./emacs.d;

  inherit sbcl;

  emacs = emacsWithPackages (epkgs:
    (with epkgs.melpaStablePackages; [
      # Lisp stuff
      paredit
      paren-face
      slime

      # Development tools
      avy
      magit
      markdown-mode
      yaml-mode
      fill-column-indicator
      highlight-indentation

      haskell-mode
      intero
      ledger-mode
      nix-mode
      rust-mode
      typescript-mode

      # General... I guess?
      discover  # contextual help menus for <?>
      powerline
      smex
      use-package
      vlf
      yasnippet
    ])
    ++
    (with epkgs.melpaPackages; [
      github-modern-theme
    ])
    ++
    (with epkgs.elpaPackages; [
      beacon  # highlight cursor on scroll
      undo-tree
    ])
  );

  builder = ./builder.sh;
}
