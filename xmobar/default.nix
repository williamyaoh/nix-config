# Produces a user configuration for xmobar, under `.xmobarrc'.

{ stdenv }:

stdenv.mkDerivation rec {
  name = "xmobar-user-${version}";
  version = "0.1.0";

  builder = ./builder.sh;

  config = ./xmobarrc;
}
