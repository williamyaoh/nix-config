# User-level overriding of Nix package settings.

{
  allowUnfree = true;
  packageOverrides = pkgs:
    let user-pkgs = import ./user-packages.nix pkgs;
    in {
         user-environment = import ./user-environment.nix (pkgs // user-pkgs);
       }
       // user-pkgs;
}
