source $stdenv/setup

mkdir -p $out/user-environment
cp $config $out/user-environment/.gitconfig
substituteInPlace $out/user-environment/.gitconfig --subst-var gitignore
