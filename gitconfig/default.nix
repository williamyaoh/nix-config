{ stdenv }:

stdenv.mkDerivation rec {
  name = "gitconfig-${version}-user";
  version = "0.2.0";

  builder = ./builder.sh;

  config = ./gitconfig;
  gitignore = ./gitignore;
}
