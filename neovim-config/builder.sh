source $stdenv/setup

mkdir -p $out/user-environment/.config

cp -r $configDir $out/user-environment/.config/nvim
