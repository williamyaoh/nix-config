set expandtab
set tabstop=2
set shiftwidth=2

set cindent

set nohlsearch
set t_Co=0
syntax off
filetype off

set ruler
set rnu

""" Keybindings

map! <M-space> <Esc>
