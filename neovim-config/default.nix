# Produce some neovim configuration under ~/.config/nvim/

{ stdenv }:

stdenv.mkDerivation rec {
  name = "neovim-config-${version}";
  version = "0.1.4";

  builder = ./builder.sh;

  configDir = ./nvim;
}
