# A set of all the overridden packages.

pkgs:

with pkgs;

let concat-files = callPackage ./concat-files {};

in rec {
  xmobar-user = callPackage ./xmobar {};

  xmonad-config =
    let xmonad-config-with-deps = callPackage ./xmonad-config {};
      in xmonad-config-with-deps {
        dmenu = dmenu-user;
        xmobar = haskellPackages.xmobar;
      };

  dmenu-user = callPackage ./dmenu {};
  gitconfig = callPackage ./gitconfig {};
  xmodmap-config = callPackage ./xmodmap-config {};
  compton-config = callPackage ./compton-config {};

  x-environment = concat-files {
    files = [ "/user-environment/.xprofile" ];
    derivations = [ xmodmap-config compton-config ];
    name = "x-environment";
  };

  emacs = let withEmacs = callPackage ./emacs-config {};
    in withEmacs {
      emacs = emacs26;
      inherit sbcl;
    };

  neovim-config = callPackage ./neovim-config {};

  kaiseki = callPackage ./kaiseki {};

  rust-nightly-platform = callPackage ./rust-nightly-nix {};

  stack-config = callPackage ./stack-config {};

  easy-ps = 
    (let pkgs = import ./easy-purescript-nix/pinned.nix {};
     in import ./easy-purescript-nix { inherit pkgs; });
}
