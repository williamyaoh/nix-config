# Launch compton when we log in.

{ compton, stdenv }:

stdenv.mkDerivation rec {
  name = "compton-config-${version}-user";
  version = "0.0.1";

  inherit compton;
  comptonFlags = ''-c -f -i 0.75 -I 0.06 -O 0.045 --focus-exclude "override_redirect = true"'';

  builder = ./builder.sh;
}
