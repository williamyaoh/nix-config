# Disparate derivations might want to write to the same configuration file;
# X configuration files, like `.xprofile' and `.Xdefaults' are big-ticket
# offenders here. We can just concatenate these files together to produce
# a working total configuration.

# The output is similar to `buildEnv' on the given derivations,
# except for the extended semantics on the concatenated files.

{ stdenv, bash }:

args@{
# Absolute paths of files (but interpreted relative to the roots of
# each derivation directory) to concatenate together.
files,

derivations,

# Name to use for the combined derivation.
name
}:

derivation (args // {
  system = builtins.currentSystem;

  builder = "${bash}/bin/bash";
  args = [ ./builder.sh ];

  inherit stdenv files derivations;
})
