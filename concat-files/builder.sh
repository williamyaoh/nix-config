source $stdenv/setup

install_dir() {
  NIX_PATH=$2
  DIR_PATH=${1#$NIX_PATH}

  mkdir -p $out/$DIR_PATH
}

install_file() {
  NIX_PATH=$2
  FILE_PATH=${1#$NIX_PATH}

  ln -sf $1 $out/$FILE_PATH
}

# Initially, just copy all the subelements in. We don't care about
# the symlink overwriting because we'll do the concatenation separately.
for derivation in $derivations; do
  for dir in $(find $derivation -type d -mindepth 1); do
    install_dir $dir $derivation
  done
  for file in $(find $derivation -type f -mindepth 1); do
    install_file $file $derivation
  done
  for file in $(find $derivation -type l -mindepth 1); do
    install_file $file $derivation
  done
done

for file in $files; do
  echo "concatenating together $out$file"
  rm -f "$out$file"
  for derivation in $derivations; do
    if [ -d "$derivation$file" ]; then
      1>&2 echo "[WARN] is a directory, but expected a file: $derivation$file"
    else
      cat "$derivation$file" >> "$out$file"
    fi
  done
done
